#include "MaterialInteractions.hh"
#include "CollimatorNew.hh"
#include "SyncPart.hh"
#include "cross_sections.hh"
#include "numrecipes.hh"
#include "OrbitConst.hh"
#include "Random.hh"

#include <iostream>
#include <cmath>
#include <cfloat>
#include <cstdlib>


// Constructor
///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   CollimatorNew::CollimatorNew
//
// DESCRIPTION
//   Constructs a collimatorNew
//
// PARAMETERS
//   length: length in m
//   ma:	 material number. (1=carbon, 2=aluminum, 3=iron, 4=copper, 5=tantalum, 6=tungstun,
//			 7=platinum, 8=lead, 9 = black absorber)
//   densityfac: density factor (for materials mixed with air or water). 1.0 for pure. 
//   shape:  shape of the collimator: 7=ellipse,
//   a:      (shape = 7) semimajor axis.
//   b:      (7) semimajor axis  
//   c:      (7) shift of right jaw
//   d:      (7) shift of left jaw
//   e:      (7) shift of bottom jaw
//   f:      (7) shift of upper jaw
//   angle:  tilt angle of collimator.
///
// RETURNS
//   int.
//
///////////////////////////////////////////////////////////////////////////

CollimatorNew::CollimatorNew(double length, int ma, 
					   double density_fac, int shape, 
					   double a, double b, double c, double d, double e, double f, double angle, double pos):
    Collimator(length, ma, density_fac, shape, a, b, c, d, angle, pos)
{
        e_ = e;
        f_ = f;
}


///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   Collimator::checkCollFlag
//
// DESCRIPTION
//   Checks to see if a particle is located inside a collimator.  Returns
//   1 if the particle is in the collimator, 0 if it isn't.
//
// PARAMETERS
//   x:      x coordinate of particle.
//   y:      y coordinate of particle.
//
// RETURNS
//   int.
//
///////////////////////////////////////////////////////////////////////////

int CollimatorNew::checkCollFlag(double x, double y){

	double xtemp = x, ytemp = y;
	double PI = OrbitConst::PI;
	double a = a_;
	double b = b_;
	double c = c_;
	double d = d_;
	double e = e_;
	double f = f_;
	double angle = angle_;
	int shape = shape_;
	double length = length_;
	
	
	if(shape==7)
    {
		if(angle != 0)
		{
			xtemp =  cos(angle*PI/180.)*x + sin(angle*PI/180.)*y;
			ytemp = -sin(angle*PI/180.)*x + cos(angle*PI/180.)*y;
		}
		if(!((pow((xtemp-c)/a, 2.0)+pow(ytemp/b, 2.0) < 1.) &&
                   (pow((xtemp-d)/a, 2.0)+pow(ytemp/b, 2.0) < 1.) &&
                   (pow(xtemp/b, 2.0)+pow((ytemp-e)/a, 2.0) < 1.) &&
                   (pow(xtemp/b, 2.0)+pow((ytemp-f)/a, 2.0) < 1.))) return 1;
    }
        return 0;
	
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   Collimator::checkStep
//
// DESCRIPTION
//   Checks to make sure that the randomly generated step size isn't too
//   large (See Catalan-Lasheras, PhD thesis, p 52).  The model is based
//   on the K2 collimation code written by Jean Bernard Jeanneret.
//
// PARAMETERS
//   
//   s:   Length of collimator remaining
//   stepsize: Randomly generated step   
//   angle:  tilt angle of collimator.
//   coords: particle coordinates
//
// RETURNS
//   Nothing
//
///////////////////////////////////////////////////////////////////////////


void CollimatorNew::checkStep(double s, double radlengthfac, double& stepsize, double* coords, SyncPart* syncpart){

	double PI = OrbitConst::PI;
	double beta = CollimatorNew::getBeta(coords, syncpart);
	double p = CollimatorNew::getP(coords, syncpart);
	double theta = 0.0136 / (beta * p) / sqrt(radlengthfac);
	double a = a_;
	double b = b_;
	double c = c_;
	double d = d_;
	double e = e_;
	double f = f_;
	double angle = angle_;
	int shape = shape_;
	double length = length_;
	
	double x = coords[0];
	double px = coords[1];
	double y = coords[2];
	double py = coords[3];
	
	float x1=0;
	float x2=length - s;
	int n=50;
	int nb=0;
	float xb1[4];
	float xb2[4];
	float solution[4];
	float smax=0.;
	float xacc=.001;
	int i;
	float r_o, r_temp, pr_o;
        int b_1=0, b_2=0, b_3=0, b_4=0;
	double arg, theta_o,  x_ellipse, y_ellipse;
	double xtemp=x, ytemp=y;
	
	
	if(shape==7)
    {
		if(angle != 0)
		{
			xtemp = cos(angle*PI/180.)*x + sin(angle*PI/180.)*y;
			ytemp = -sin(angle*PI/180.)*x + cos(angle*PI/180.)*y;
		}
                if(xtemp >= 0 && ytemp >=0) b_1 = 1;
                if(xtemp <= 0 && ytemp >=0) b_2 = 1;
                if(xtemp <= 0 && ytemp <=0) b_3 = 1;
                if(xtemp >= 0 && ytemp <=0) b_4 = 1;
                r_o = sqrt(pow(xtemp, 2) + pow(ytemp, 2));//initialize a value
                if(b_1 == 1 || b_4 == 1)
                {
		arg = abs(ytemp/(xtemp-c));
		theta_o = atan(arg);
		y_ellipse = sqrt(pow( a*tan(theta_o) , 2.0)/(1+pow( a*tan(theta_o)/b , 2.0)));
		x_ellipse = c + sqrt(a*a - pow( a*y_ellipse/b, 2.0));
		r_temp = sqrt(xtemp*xtemp + ytemp*ytemp) - 
		sqrt(x_ellipse*x_ellipse + y_ellipse*y_ellipse);
                r_o = (r_o < r_temp) ? r_o : r_temp;
                }

                if(b_2 == 1 || b_3 == 1)
                {
		arg = abs(ytemp/(xtemp-d));
		theta_o = atan(arg);
		y_ellipse = sqrt(pow( a*tan(theta_o) , 2.0)/(1+pow( a*tan(theta_o)/b , 2.0)));
		x_ellipse = d + sqrt(a*a - pow( a*y_ellipse/b, 2.0));
		r_temp = sqrt(xtemp*xtemp + ytemp*ytemp) - 
		sqrt(x_ellipse*x_ellipse + y_ellipse*y_ellipse);
                r_o = (r_o < r_temp) ? r_o : r_temp;
                }

                if(b_1 == 1 || b_2 == 1)
                {
		arg = abs(xtemp/(ytemp-e));
		theta_o = atan(arg);
		x_ellipse = sqrt(pow( a*tan(theta_o) , 2.0)/(1+pow( a*tan(theta_o)/b , 2.0)));
		y_ellipse = e + sqrt(a*a - pow( a*x_ellipse/b, 2.0));
		r_temp = sqrt(xtemp*xtemp + ytemp*ytemp) - 
		sqrt(x_ellipse*x_ellipse + y_ellipse*y_ellipse);
                r_o = (r_o < r_temp) ? r_o : r_temp;
                }

                if(b_3 == 1 || b_4 == 1)
                {
		arg = abs(xtemp/(ytemp-f));
		theta_o = atan(arg);
		x_ellipse = sqrt(pow( a*tan(theta_o) , 2.0)/(1+pow( a*tan(theta_o)/b , 2.0)));
		y_ellipse = f + sqrt(a*a - pow( a*x_ellipse/b, 2.0));
		r_temp = sqrt(xtemp*xtemp + ytemp*ytemp) - 
		sqrt(x_ellipse*x_ellipse + y_ellipse*y_ellipse);
                r_o = (r_o < r_temp) ? r_o : r_temp;
                }
    }	       
	
	pr_o = (x*px + y*py)/sqrt(x*x+y*y);

	for(i=0; i<4; i++)
    {
		xb1[i]=0;
		xb2[i]=0;
    }
	
	OrbitUtils::zbrak(OrbitUtils::fstep, x1, x2, n, xb1, xb2, nb, r_o, pr_o, theta);
	
	for(i=1; i<=nb; i++)
    {
		solution[i]=OrbitUtils::rtbis(OrbitUtils::fstep, xb1[i], xb2[i], xacc, r_o, pr_o, theta);
    }
	
	solution[0]=100000.;
	
	for(i=1; i<=nb; i++)
		if(solution[i] < solution[i-1])
		{
			smax=solution[i];
		}
	
	if(smax > 0 && stepsize > smax){
		
	 stepsize=smax;
	 
	 }

}	

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   CollimatorNew::getBeta
//
// DESCRIPTION
//   Gets current beta for particle
//
// PARAMETERS
//
//	 coords: particle coordinates
//   syncpart: the relevant synchronous particle
//
// RETURNS
//   double.
//
///////////////////////////////////////////////////////////////////////////

double CollimatorNew::getBeta(double* coords, SyncPart* syncpart){
	
	double T = syncpart->getEnergy();
	double M = syncpart->getMass();
	double T_part = T + coords[5];
	double E_part = T_part + M;
	double beta = sqrt((E_part*E_part - M * M))/E_part;

	return beta;
	
}

///////////////////////////////////////////////////////////////////////////
//
// NAME
//
//   CollimatorNew::getP
//
// DESCRIPTION
//   Gets current momentum of particle
//
// PARAMETERS
//
//	 coords: particle coordinates
//   syncpart: the relevant synchronous particle
//
// RETURNS
//   double.
//
///////////////////////////////////////////////////////////////////////////

double CollimatorNew::getP(double* coords, SyncPart* syncpart){

	double T = syncpart->getEnergy();
	double M = syncpart->getMass();
	double T_part = T + coords[5];
	double E_part = T_part + M;
	double p_part = sqrt((E_part*E_part - M * M));
	
	return p_part;
	
}

