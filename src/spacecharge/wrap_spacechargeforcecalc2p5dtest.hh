#ifndef WRAP_SPACE_CHARGE_FORCE_CALC_2P5D_TEST_H
#define WRAP_SPACE_CHARGE_FORCE_CALC_2P5D_TEST_H

#include "Python.h"

#ifdef __cplusplus
extern "C" {
#endif

  namespace wrap_spacecharge{
    void initSpaceChargeForceCalc2p5DTEST(PyObject* module);
  }
	
#ifdef __cplusplus
}
#endif // __cplusplus

#endif // WRAP_SPACE_CHARGE_FORCE_CALC_2P5D_TEST_H
